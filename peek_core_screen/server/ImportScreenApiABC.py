from abc import ABCMeta, abstractmethod
from peek_core_screen._private.storage import ScreenTuple


class ImportScreenApiABC(metaclass=ABCMeta):
    @abstractmethod
    def doSomethingGood(self, somethingsDescription: str) -> ScreenTuple:
        """TODO"""
