from typing import List

from vortex.Tuple import addTupleType, TupleField, Tuple
from peek_core_screen.tuples.style import ContainerStyleTuple
from peek_core_screen._private.PluginNames import screenTuplePrefix


@addTupleType
class ContainerWidgetTuple(Tuple):
    """ContainerWidget Tuple

    This tuple represents a container element in Screen
    """

    __tupleType__ = screenTuplePrefix + "ContainerWidgetTuple"

    key: str = TupleField()
    title: str = TupleField()
    isLeaf: bool = TupleField()
    children: List = TupleField([])

    orientation: int = TupleField()
    horizontalAlign: int = TupleField()
    verticalAlign: int = TupleField()
    columns: int = TupleField()

    styleKey: str = TupleField()
