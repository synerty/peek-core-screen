from vortex.Tuple import addTupleType, TupleField, Tuple
from peek_core_screen.tuples.style.TextStyleTuple import TextStyleTuple
from peek_core_screen._private.PluginNames import screenTuplePrefix


@addTupleType
class TextWidgetTuple(Tuple):
    """ContainerWidget Tuple

    This tuple represents a container element in Screen
    """

    __tupleType__ = screenTuplePrefix + "TextWidgetTuple"

    key: str = TupleField()
    title: str = TupleField()
    isLeaf: bool = TupleField()
    children: list = TupleField()

    label: str = TupleField()
    bindText: str = TupleField()

    styleKey: str = TupleField()
