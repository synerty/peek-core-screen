from vortex.Tuple import addTupleType, TupleField, Tuple
from peek_core_screen._private.PluginNames import screenTuplePrefix


@addTupleType
class ContainerStyleTuple(Tuple):
    """ContainerWidget Tuple

    This tuple represents a container element in Screen
    """

    __tupleType__ = screenTuplePrefix + "ContainerStyleTuple"

    backgroundColor: str = TupleField()
    fontFamily: str = TupleField()
    fontSize: str = TupleField()
    borderStyle: str = TupleField()
    borderWidth: str = TupleField()
    borderColor: str = TupleField()
    padding: str = TupleField()
    margin: str = TupleField()
