from vortex.Tuple import addTupleType, TupleField, Tuple
from peek_core_screen._private.PluginNames import screenTuplePrefix


@addTupleType
class TextStyleTuple(Tuple):
    """ContainerWidget Tuple

    This tuple represents a container element in Screen
    """

    __tupleType__ = screenTuplePrefix + "TextStyleTuple"

    underline: bool = TupleField()
    bold: bool = TupleField()
    italic: bool = TupleField()
