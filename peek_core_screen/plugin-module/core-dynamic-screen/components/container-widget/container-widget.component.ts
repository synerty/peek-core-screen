import {
    ChangeDetectionStrategy,
    Component,
    ComponentRef,
    Input,
    OnDestroy,
    OnInit,
    ViewChild,
    ViewContainerRef,
} from "@angular/core";
import {
    ContainerStyleTuple,
    ContainerWidgetOrientation,
    ContainerWidgetTuple,
    StyleTuple,
} from "@peek/peek_core_screen/_private";
import { CoreDynamicInterpolationService } from "../../services/core-dynamic-interpolation.service";
import { BehaviorSubject } from "rxjs";

@Component({
    selector: "screen-container-widget",
    templateUrl: "container-widget.component.html",
    styleUrls: ["container-widget.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerWidgetComponent implements OnInit, OnDestroy {
    @Input()
    data: any;
    @Input()
    styleTuple: StyleTuple;
    @Input()
    widgetTuple: ContainerWidgetTuple;

    @ViewChild("verticalContainerChildren", {
        read: ViewContainerRef,
        static: false,
    })
    verticalContainerTarget: ViewContainerRef;
    @ViewChild("horizontalContainerChildren", {
        read: ViewContainerRef,
        static: false,
    })
    horizontalContainerTarget: ViewContainerRef;

    componentRefs: ComponentRef<any>[] = [];
    interpolate: CoreDynamicInterpolationService;

    style$ = new BehaviorSubject<ContainerStyleTuple>(null);

    constructor(interpolate: CoreDynamicInterpolationService) {
        this.interpolate = interpolate;
    }

    get style() {
        return this.style$.getValue();
    }

    set style(value) {
        this.style$.next(value);
    }

    ngOnInit() {
        if (this.styleTuple) {
            this.styleTuple.decodeJsonData().then((jsonData: any) => {
                console.log(jsonData);
                this.style = jsonData;
            });
        }
    }

    isHorizontal(): boolean {
        return (
            this.widgetTuple.orientation == null ||
            this.widgetTuple.orientation ==
                ContainerWidgetOrientation.Horizontal
        );
    }

    isVertical(): boolean {
        return (
            this.widgetTuple.orientation == ContainerWidgetOrientation.Vertical
        );
    }

    ngOnDestroy() {
        for (let i = 0; i < this.componentRefs.length; i++) {
            this.componentRefs[i].destroy();
        }
        this.componentRefs = [];
    }
}
