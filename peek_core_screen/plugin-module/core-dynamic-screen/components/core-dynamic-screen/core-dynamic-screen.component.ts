import { BehaviorSubject, Subscription } from "rxjs";
import { takeUntil } from "rxjs/operators";
import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Input,
    SimpleChanges,
    ViewChild,
    ViewContainerRef,
} from "@angular/core";
import {
    ContainerWidgetTuple,
    ScreenTuple,
    ScreenTupleService,
} from "@peek/peek_core_screen/_private";
import { NgLifeCycleEvents, TupleSelector } from "@synerty/vortexjs";
import { CoreDynamicScreenService } from "../../services/core-dynamic-screen.service";

@Component({
    selector: "core-dynamic-screen",
    templateUrl: "core-dynamic-screen.component.html",
    styleUrls: ["core-dynamic-screen.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CoreDynamicScreenComponent extends NgLifeCycleEvents {
    @Input()
    screenTupleByKey: { [key: string]: string };
    @Input()
    data: any;

    @ViewChild("root", { read: ViewContainerRef, static: false })
    target: ViewContainerRef;

    private rootWidgetTuple = new BehaviorSubject<ContainerWidgetTuple>(null);
    private screenTuple: ScreenTuple;
    private screenTupleSubscription: Subscription;
    private rootWidgetSubscription: Subscription;
    private rootComponentRef;

    constructor(
        private tupleService: ScreenTupleService,
        private coreDynamicScreenService: CoreDynamicScreenService,
        private changeDetectorRef: ChangeDetectorRef
    ) {
        super();
    }

    /* On creations of the root Widget, create the sub widget */
    ngOnInit() {
        /* Unsubscribe if previously subscribed */
        this.unSubscribeRootWidgetSubscription();

        /* Create the component */
        this.rootWidgetSubscription = this.rootWidgetTuple.subscribe(
            (rootWidgetTuple) => {
                this.destroyComponentRef();

                if (rootWidgetTuple) {
                    this.rootComponentRef =
                        this.coreDynamicScreenService.populateComponents(
                            this.target,
                            rootWidgetTuple,
                            this.screenTuple.styleByKey,
                            this.data
                        );
                }
            }
        );
    }

    ngOnChanges(change: SimpleChanges) {
        if (this.screenTupleByKey) {
            this.loadScreenTuple();
        }
    }

    /* On Destroying the component unsubscribe and destroy dynamic components */
    ngOnDestroy() {
        this.unSubscribeRootWidgetSubscription();
        this.destroyComponentRef();
    }

    /* Unsubscribe root widget subscrption */
    private unSubscribeRootWidgetSubscription(): void {
        if (this.rootWidgetSubscription) {
            this.rootWidgetSubscription.unsubscribe();
            this.rootWidgetSubscription = null;
        }
    }

    /* Destroy previous components */
    private destroyComponentRef(): void {
        if (this.rootComponentRef) {
            this.rootComponentRef.destroy();
            this.rootComponentRef = null;
        }
    }

    /* Load Screen Tuple */
    private loadScreenTuple(): void {
        const screenTupleSelector = new TupleSelector(ScreenTuple.tupleName, {
            key: this.screenTupleByKey.key,
        });

        if (this.screenTupleSubscription) {
            this.screenTupleSubscription.unsubscribe();
            this.screenTupleSubscription = null;
        }

        this.screenTupleSubscription = this.tupleService.observer
            .subscribeToTupleSelector(screenTupleSelector)
            .pipe(takeUntil(this.onDestroyEvent))
            .subscribe((tuples: ScreenTuple[]) => {
                if (tuples.length > 0) {
                    this.screenTuple = tuples[0];
                    this.screenTuple
                        .decodeContainerWidget()
                        .then((widget: any) => {
                            this.rootWidgetTuple.next(widget);
                        });
                }
            });
    }
}
