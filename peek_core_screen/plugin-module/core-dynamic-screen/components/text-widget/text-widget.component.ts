import {
    ChangeDetectionStrategy,
    Component,
    ComponentRef,
    Input,
    OnDestroy,
    OnInit,
} from "@angular/core";
import { CoreDynamicInterpolationService } from "../../services/core-dynamic-interpolation.service";
import {
    StyleTuple,
    TextStyleTuple,
    TextWidgetTuple,
} from "@peek/peek_core_screen/_private";

@Component({
    selector: "screen-text-widget",
    templateUrl: "text-widget.component.html",
    styleUrls: ["text-widget.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextWidgetComponent implements OnInit, OnDestroy {
    @Input()
    data: any;
    @Input()
    styleTuple: StyleTuple;
    @Input()
    widgetTuple: TextWidgetTuple;

    componentRefs: ComponentRef<any>[] = [];
    interpolate: CoreDynamicInterpolationService;
    style: TextStyleTuple;

    constructor(interpolate: CoreDynamicInterpolationService) {
        this.interpolate = interpolate;
    }

    ngOnInit() {
        if (this.styleTuple) {
            this.styleTuple.decodeJsonData().then((jsonData: any) => {
                this.style = jsonData;
            });
        }
    }

    ngOnDestroy() {
        for (let i = 0; i < this.componentRefs.length; i++) {
            this.componentRefs[i].destroy();
        }
        this.componentRefs = [];
    }
}
