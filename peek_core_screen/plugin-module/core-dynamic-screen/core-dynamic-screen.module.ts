import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NzGridModule } from "ng-zorro-antd/grid";
import { NzFormModule } from "ng-zorro-antd/form";
import { NzDividerModule } from "ng-zorro-antd/divider";
import { FormsModule } from "@angular/forms";
import { NzIconModule } from "ng-zorro-antd/icon";
import { RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { CoreDynamicScreenComponent } from "./components/core-dynamic-screen/core-dynamic-screen.component";
import { ContainerWidgetComponent } from "./components/container-widget/container-widget.component";
import { TextWidgetComponent } from "./components/text-widget/text-widget.component";
import { CoreDynamicScreenService } from "./services/core-dynamic-screen.service";
import { CoreDynamicInterpolationService } from "./services/core-dynamic-interpolation.service";

@NgModule({
    imports: [
        CommonModule,
        NzGridModule,
        NzDividerModule,
        NzFormModule,
        RouterModule,
        FormsModule,
        NzIconModule,
        HttpClientModule,
    ],
    exports: [CoreDynamicScreenComponent],
    providers: [CoreDynamicScreenService, CoreDynamicInterpolationService],
    declarations: [
        CoreDynamicScreenComponent,
        ContainerWidgetComponent,
        TextWidgetComponent,
    ],
    entryComponents: [ContainerWidgetComponent, TextWidgetComponent],
})
export class CoreDynamicScreenModule {}
