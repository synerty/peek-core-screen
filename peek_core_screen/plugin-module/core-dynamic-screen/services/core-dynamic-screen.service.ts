import {
    ComponentFactoryResolver,
    Injectable,
    ViewContainerRef,
} from "@angular/core";
import { COMPONENT_CONFIGS } from "../constants";
import {
    ContainerWidgetOrientation,
    ContainerWidgetTuple,
} from "../../_private";

@Injectable({
    providedIn: "root",
})
export class CoreDynamicScreenService {
    private componentConfigs = COMPONENT_CONFIGS;

    constructor(private componentFactoryResolver: ComponentFactoryResolver) {}

    /* Create component given tuple and data to bind,
     returns the reference to the component */
    createComponent(
        component: any,
        viewContainerRef: ViewContainerRef,
        widgetTuple: any,
        styleByKey: { [key: string]: any },
        data: any
    ) {
        /* Get the component dynamically and create a factory */
        const componentFactory =
            this.componentFactoryResolver.resolveComponentFactory(component);

        /* Create component */
        const componentRef = viewContainerRef.createComponent(componentFactory);

        /* Pass the data */
        componentRef.instance["widgetTuple"] = widgetTuple;
        componentRef.instance["data"] = data;
        // Set style
        if (
            widgetTuple.styleKey &&
            styleByKey.hasOwnProperty(widgetTuple.styleKey)
        ) {
            componentRef.instance["styleTuple"] =
                styleByKey[widgetTuple.styleKey];
        }
        componentRef.changeDetectorRef.detectChanges();

        return componentRef;
    }

    /* Generate all the components recursively */
    populateComponents(
        target: ViewContainerRef,
        widgetTuple: any,
        styleByKey: { [key: string]: any },
        data: any
    ) {
        /* Get TupleName, Tuple mapped Component */
        const tupleName = widgetTuple._tupleName();
        const component = this.componentConfigs[tupleName].component;

        /* Calculate the total weight */
        let totalColumnWeights = 0;
        if (
            tupleName == ContainerWidgetTuple.tupleName &&
            widgetTuple.orientation == ContainerWidgetOrientation.Horizontal
        ) {
            totalColumnWeights = this.calculateTotalColumnsWeight(widgetTuple);
        }

        /* Create component instance and get Target Reference and Children Tuples */
        const componentRef = this.createComponent(
            component,
            target,
            widgetTuple,
            styleByKey,
            data
        );

        const parentTarget = COMPONENT_CONFIGS[tupleName].getTarget(
            componentRef.instance
        );
        const childrenTuples = widgetTuple.children;

        if (parentTarget && childrenTuples) {
            for (const childWidgetTuple of childrenTuples) {
                if (
                    childWidgetTuple._tupleName() ==
                        ContainerWidgetTuple.tupleName &&
                    childWidgetTuple.orientation ==
                        ContainerWidgetOrientation.Vertical
                ) {
                    childWidgetTuple.totalCol = totalColumnWeights;
                }
                const ref = this.populateComponents(
                    parentTarget,
                    childWidgetTuple,
                    styleByKey,
                    data
                );
                componentRef.instance["componentRefs"].push(ref);
            }
        }

        return componentRef;
    }

    /* Total the weights of ContainerWidgetTuple with Vertical Orientation */
    private calculateTotalColumnsWeight(widgetTuple: any): number {
        let total = 0;
        if (widgetTuple.children) {
            for (const childWidgeTuple of widgetTuple.children) {
                if (
                    childWidgeTuple.orientation ==
                    ContainerWidgetOrientation.Vertical
                ) {
                    total += childWidgeTuple.columns;
                }
            }
        }
        return total;
    }
}
