import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root",
})
export class CoreDynamicInterpolationService {
    /* Get Value from data for the given key else return default */
    getVal(key: string, data: object): string | number | null {
        if (!key) {
            return "";
        }
        const val = this.byString(data, key);
        return val ? val : key;
    }

    // TODO: Implement setVal

    // Get index by string
    private byString(obj, str) {
        // convert indexes to properties
        str = str.replace(/\[(\w+)\]/g, ".$1");
        // strip a leading dot
        str = str.replace(/^\./, "");
        let a = str.split(".");
        for (let i = 0, n = a.length; i < n; ++i) {
            let k = a[i];

            if (!obj) {
                return null;
            } else if (k in obj) {
                obj = obj[k];
            } else {
                return null;
            }
        }
        return obj;
    }
}
