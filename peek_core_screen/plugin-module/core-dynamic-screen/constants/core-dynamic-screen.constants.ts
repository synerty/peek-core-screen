import { TextWidgetComponent } from "../components/text-widget/text-widget.component";
import { ContainerWidgetComponent } from "../components/container-widget/container-widget.component";

const COMPONENT_CONFIGS: object = {
    "peek_core_screen.ContainerWidgetTuple": {
        component: ContainerWidgetComponent,
        getTarget: (containerWidgetComponent: ContainerWidgetComponent) => {
            if (containerWidgetComponent.isVertical())
                return containerWidgetComponent.verticalContainerTarget;
            else return containerWidgetComponent.horizontalContainerTarget;
        },
    },
    "peek_core_screen.TextWidgetTuple": {
        component: TextWidgetComponent,
        getTarget: (textWidgetComponent: TextWidgetComponent) => null,
    },
};

export { COMPONENT_CONFIGS };
