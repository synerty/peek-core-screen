export let screenFilt = { plugin: "peek_core_screen" };
export let screenTuplePrefix = "peek_core_screen.";

export let screenObservableName = "peek_core_screen";
export let screenActionProcessorName = "peek_core_screen";
export let screenTupleOfflineServiceName = "peek_core_screen";

export let screenBaseUrl = "peek_core_screen";
