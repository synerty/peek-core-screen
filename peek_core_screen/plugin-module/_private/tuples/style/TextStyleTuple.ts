import { addTupleType, Tuple } from "@synerty/vortexjs";
import { screenTuplePrefix } from "../../PluginNames";

@addTupleType
export class TextStyleTuple extends Tuple {
    public static readonly tupleName = screenTuplePrefix + "TextStyleTuple";
    underline: boolean;
    bold: boolean;
    italic: boolean;

    constructor() {
        super(TextStyleTuple.tupleName);
    }

    get getBold() {
        return this.bold === true ? "bold" : "normal";
    }

    get getUnderline() {
        return this.underline === true ? "underline" : "none";
    }

    get getItalic() {
        return this.italic === true ? "italic" : "normal";
    }
}
