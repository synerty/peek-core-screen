import { addTupleType, Tuple } from "@synerty/vortexjs";
import { screenTuplePrefix } from "../../PluginNames";

@addTupleType
export class ContainerStyleTuple extends Tuple {
    public static readonly tupleName =
        screenTuplePrefix + "ContainerStyleTuple";
    backgroundColor: string;
    fontFamily: string;
    fontSize: string;
    borderStyle: string;
    borderWidth: string;
    borderColor: string;
    padding: string;
    margin: string;

    constructor() {
        super(ContainerStyleTuple.tupleName);
    }

    get getBackgroundColor(): string {
        return this.backgroundColor;
    }

    get getFontSize(): string {
        if (isNaN(Number(this.fontSize)) == false) {
            return this.fontSize + "px";
        } else {
            return this.fontSize;
        }
    }
}
