import { addTupleType, Tuple } from "@synerty/vortexjs";
import { screenTuplePrefix } from "../../PluginNames";

@addTupleType
export class DatetimeStyleTuple extends Tuple {
    public static readonly tupleName = screenTuplePrefix + "DatetimeStyleTuple";

    constructor() {
        super(DatetimeStyleTuple.tupleName);
    }
}
