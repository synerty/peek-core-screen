import { addTupleType, Tuple } from "@synerty/vortexjs";
import { screenTuplePrefix } from "../../PluginNames";

@addTupleType
export class CheckboxStyleTuple extends Tuple {
    public static readonly tupleName = screenTuplePrefix + "CheckboxStyleTuple";

    constructor() {
        super(CheckboxStyleTuple.tupleName);
    }
}
