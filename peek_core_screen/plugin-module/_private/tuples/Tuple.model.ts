import { TextWidgetTuple } from "./widget/TextWidgetTuple";
import {
    ContainerWidgetOrientation,
    ContainerWidgetTuple,
} from "./widget/ContainerWidgetTuple";
import { ContainerStyleTuple } from "./style/ContainerStyleTuple";
import { TextStyleTuple } from "./style/TextStyleTuple";

/* Tuple Model holds the key names of Style and Widget Tuples and its properties */
const TupleModel = {
    widgets: {
        "peek_core_screen.ContainerWidgetTuple": {
            label: "Container",
            create: () => {
                const containerWidgetTuple = new ContainerWidgetTuple();
                containerWidgetTuple.orientation =
                    ContainerWidgetOrientation.Horizontal;
                return containerWidgetTuple;
            },
            style: "peek_core_screen.ContainerStyleTuple",
        },
        "peek_core_screen.TextWidgetTuple": {
            label: "Text",
            create: () => new TextWidgetTuple(),
            style: "peek_core_screen.TextStyleTuple",
        },
    },
    styles: {
        "peek_core_screen.ContainerStyleTuple": {
            label: "Container",
            create: () => new ContainerStyleTuple(),
        },
        "peek_core_screen.TextStyleTuple": {
            label: "Text",
            create: () => new TextStyleTuple(),
        },
    },
};

export { TupleModel };
