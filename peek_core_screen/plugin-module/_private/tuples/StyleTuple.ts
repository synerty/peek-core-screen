import { addTupleType, Payload, Tuple } from "@synerty/vortexjs";
import { screenTuplePrefix } from "../PluginNames";
import { ContainerStyleTuple } from "./style/ContainerStyleTuple";
import { DatetimeStyleTuple } from "./style/DatetimeStyleTuple";
import { TextStyleTuple } from "./style/TextStyleTuple";
import { CheckboxStyleTuple } from "./style/CheckboxStyleTuple";

@addTupleType
export class StyleTuple extends Tuple {
    public static readonly tupleName = screenTuplePrefix + "StyleTuple";
    id: number;
    key: string;
    name: string;
    widgetType: string;
    jsonData: string;

    constructor() {
        super(StyleTuple.tupleName);
    }

    /* Encode the Style Tuple */
    static async encodeJsonData(
        styleTuple:
            | ContainerStyleTuple
            | DatetimeStyleTuple
            | TextStyleTuple
            | CheckboxStyleTuple
    ): Promise<string> {
        return await new Payload({}, [styleTuple]).toEncodedPayload();
    }

    /* Decode the String to Style Tuple */
    async decodeJsonData(): Promise<StyleTuple> {
        if (this.jsonData == null) {
            return null;
        }
        const payload = await Payload.fromEncodedPayload(this.jsonData);
        if (payload.tuples && payload.tuples.length > 0) {
            return payload.tuples[0];
        }
        return null;
    }
}
