import { addTupleType, Tuple } from "@synerty/vortexjs";
import { screenTuplePrefix } from "../../PluginNames";
import { TextStyleTuple } from "../..";

@addTupleType
export class TextWidgetTuple extends Tuple {
    public static readonly tupleName = screenTuplePrefix + "TextWidgetTuple";
    key: string;
    title: string;

    label: string;
    bindText: string;

    styleKey: string;
    styleTuple: TextStyleTuple | null;

    constructor() {
        super(TextWidgetTuple.tupleName);
    }

    get isLeaf(): boolean {
        return true;
    }
}
