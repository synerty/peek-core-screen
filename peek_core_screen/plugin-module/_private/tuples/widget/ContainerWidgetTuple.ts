import { addTupleType, Tuple } from "@synerty/vortexjs";
import { TextWidgetTuple } from "./TextWidgetTuple";
import { screenTuplePrefix } from "../../PluginNames";

/* Enums */
export enum ContainerWidgetOrientation {
    Horizontal,
    Vertical,
}

export enum ContainerWidgetVerticalAlignment {
    Left,
    Center,
    Right,
}

export enum ContainerWidgetHorizontalAlighment {
    Top,
    Middle,
    Bottom,
}

const totalGridSize = 24;
const gridAllowedValues = [1, 2, 3, 4, 6, 8, 12, 24];

function getGridValue(gridValue: number, extraSteps: number): number {
    let i = 0;
    while (i < gridAllowedValues.length) {
        if (gridValue <= gridAllowedValues[i]) {
            break;
        }
        i++;
    }

    i += extraSteps;
    if (i < 0) {
        return gridAllowedValues[0];
    }
    if (i >= gridAllowedValues.length) {
        return gridAllowedValues[gridAllowedValues.length - 1];
    }
    return gridAllowedValues[i];
}

/* Container Widget Tuple */
@addTupleType
export class ContainerWidgetTuple extends Tuple {
    public static readonly tupleName =
        screenTuplePrefix + "ContainerWidgetTuple";

    key: string;
    title: string;
    children: Array<ContainerWidgetTuple | TextWidgetTuple>;

    orientation: ContainerWidgetOrientation;
    horizontalAlign: ContainerWidgetHorizontalAlighment;
    verticalAlign: ContainerWidgetVerticalAlignment;
    columns: number;
    totalCol: number;

    styleKey: string;

    constructor() {
        super(ContainerWidgetTuple.tupleName);
    }

    get isLeaf(): boolean {
        return false;
    }

    get getXs(): number {
        const val = (this.columns / this.totalCol) * totalGridSize;
        return getGridValue(val, 3);
    }

    get getSm(): number {
        const val = (this.columns / this.totalCol) * totalGridSize;
        return getGridValue(val, 2);
    }

    get getMd(): number {
        const val = (this.columns / this.totalCol) * totalGridSize;
        return getGridValue(val, 1);
    }

    get getLg(): number {
        const val = (this.columns / this.totalCol) * totalGridSize;
        return getGridValue(val, 0);
    }

    get getXl(): number {
        const val = (this.columns / this.totalCol) * totalGridSize;
        return getGridValue(val, 0);
    }

    get getXXl(): number {
        const val = (this.columns / this.totalCol) * totalGridSize;
        return getGridValue(val, -1);
    }

    static createRootContainerWidget(
        key: string,
        title: string
    ): ContainerWidgetTuple {
        const rootContainerWidget = new ContainerWidgetTuple();
        rootContainerWidget.key = key;
        rootContainerWidget.title = title;
        rootContainerWidget.orientation = ContainerWidgetOrientation.Horizontal;
        return rootContainerWidget;
    }
}
