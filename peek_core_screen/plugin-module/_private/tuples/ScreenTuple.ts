import { addTupleType, Payload, Tuple } from "@synerty/vortexjs";
import { screenTuplePrefix } from "../PluginNames";
import { ContainerWidgetTuple } from "./widget/ContainerWidgetTuple";

@addTupleType
export class ScreenTuple extends Tuple {
    public static readonly tupleName = screenTuplePrefix + "ScreenTuple";
    public static readonly detailScreen = "DetailScreen";
    id: number;
    key: string;
    name: string;
    screenType: string;
    encodedContainerWidget: string;
    styleByKey: { [key: string]: any } | null;

    constructor() {
        super(ScreenTuple.tupleName);
    }

    /* Create a new Screen Tuple */
    static async createDetailScreenTuple(
        key: string,
        name: string
    ): Promise<ScreenTuple> {
        const screenTuple = new ScreenTuple();
        screenTuple.name = name;
        screenTuple.key = key;
        screenTuple.screenType = ScreenTuple.detailScreen;
        const containerWidgetTuple = new ContainerWidgetTuple();
        containerWidgetTuple.key = key;
        containerWidgetTuple.title = name;
        containerWidgetTuple.children = [];
        screenTuple.encodedContainerWidget =
            await ScreenTuple.encodeContainerWidget(containerWidgetTuple);
        return screenTuple;
    }

    /* Encode the Widget Tuples */
    static async encodeContainerWidget(
        containerWidget: ContainerWidgetTuple
    ): Promise<string> {
        return await new Payload({}, [containerWidget]).toEncodedPayload();
    }

    /* Decode the String to Widgets */
    async decodeContainerWidget(): Promise<ContainerWidgetTuple | null> {
        if (this.encodedContainerWidget == null) {
            return null;
        }

        const payload = await Payload.fromEncodedPayload(
            this.encodedContainerWidget
        );
        if (payload.tuples && payload.tuples.length > 0) {
            return payload.tuples[0];
        }
        return null;
    }
}
