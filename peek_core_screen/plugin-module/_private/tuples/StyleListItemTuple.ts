import { addTupleType, Tuple } from "@synerty/vortexjs";
import { screenTuplePrefix } from "../PluginNames";

@addTupleType
export class StyleListItemTuple extends Tuple {
    public static readonly tupleName = screenTuplePrefix + "StyleListItemTuple";
    id: number;
    key: string;
    name: string;
    widgetType: string;

    constructor() {
        super(StyleListItemTuple.tupleName);
    }
}
