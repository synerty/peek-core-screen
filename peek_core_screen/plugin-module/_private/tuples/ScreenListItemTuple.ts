import { addTupleType, Tuple } from "@synerty/vortexjs";
import { screenTuplePrefix } from "../PluginNames";

@addTupleType
export class ScreenListItemTuple extends Tuple {
    public static readonly tupleName =
        screenTuplePrefix + "ScreenListItemTuple";
    id: number;
    key: string;
    name: string;
    screenType: string;

    constructor() {
        super(ScreenListItemTuple.tupleName);
    }
}
