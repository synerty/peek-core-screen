import { Injectable } from "@angular/core";
import {
    TupleActionPushNameService,
    TupleActionPushService,
    TupleDataObservableNameService,
    TupleDataObserverService,
    TupleDataOfflineObserverService,
    TupleOfflineStorageService,
    VortexService,
    VortexStatusService,
} from "@synerty/vortexjs";
import {
    screenActionProcessorName,
    screenFilt,
    screenObservableName,
} from "./PluginNames";

export function tupleDataObservableNameServiceFactory() {
    return new TupleDataObservableNameService(screenObservableName, screenFilt);
}

export function tupleActionPushNameServiceFactory() {
    return new TupleActionPushNameService(
        screenActionProcessorName,
        screenFilt
    );
}

export interface TupleLoaderFilterI {
    id: number;
    key: string;
}

@Injectable({
    providedIn: "root",
})
export class ScreenTupleService {
    public observer: TupleDataObserverService;
    public action: TupleActionPushService;
    public offlineObserver: TupleDataOfflineObserverService;
    public offlineStorage: TupleOfflineStorageService;

    constructor(
        vortexService: VortexService,
        vortexStatusService: VortexStatusService
    ) {
        // Online Actions
        this.action = new TupleActionPushService(
            tupleActionPushNameServiceFactory(),
            vortexService,
            vortexStatusService
        );

        // Register the observer
        let observerName = tupleDataObservableNameServiceFactory();
        this.offlineObserver = new TupleDataOfflineObserverService(
            vortexService,
            vortexStatusService,
            observerName,
            this.offlineStorage
        );
        this.observer = new TupleDataObserverService(
            this.offlineObserver,
            observerName
        );
    }
}
