==============
Administration
==============
.. toctree::
    :maxdepth: 2
    :caption: Contents:

.. note:: This plugin feature is under development.

.. image:: flow_diagram.png
    :align: center

Dynamic Screen Configuration
----------------------------
Peek Core Screen Plugin is used to configure screen for the peek-field and the peek-office

Organise the containers and widgets, apply styles and customisation

.. image:: detail_screen.png

Details Screen
--------------

TODO

Styling
-------



