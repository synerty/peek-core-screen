import logging

from peek_core_screen._private.PluginNames import screenTuplePrefix
from sqlalchemy import Column, BigInteger
from sqlalchemy import String
from sqlalchemy.sql.schema import Index

from vortex.Tuple import Tuple, addTupleType, TupleField
from .DeclarativeBase import DeclarativeBase

logger = logging.getLogger(__name__)


@addTupleType
class ScreenTuple(Tuple, DeclarativeBase):
    """StyleTuple

    This table stores Screen customisation.

    """

    __tablename__ = "ScreenTuple"
    __tupleType__ = screenTuplePrefix + "ScreenTuple"

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    key = Column(String(50), nullable=False)
    name = Column(String(50), nullable=False)
    screenType = Column(String, nullable=False)
    encodedContainerWidget = Column(String, nullable=False)
    styleByKey = TupleField({})

    __table_args__ = (Index("idx_ScreenTuple_key", key, unique=True),)
