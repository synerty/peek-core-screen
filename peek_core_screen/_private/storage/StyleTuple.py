import logging

from peek_core_screen._private.PluginNames import screenTuplePrefix
from sqlalchemy import Column, BigInteger
from sqlalchemy import String
from sqlalchemy.sql.schema import Index

from vortex.Tuple import Tuple, addTupleType
from .DeclarativeBase import DeclarativeBase

logger = logging.getLogger(__name__)


@addTupleType
class StyleTuple(Tuple, DeclarativeBase):
    """StyleTuple

    This table stores all the Styles that are required for rendering the Dynamic Screen.
    Note: The Style is agnostic of any specific language or markup.

    """

    __tablename__ = "StyleTuple"
    __tupleType__ = screenTuplePrefix + "StyleTuple"

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    key = Column(String(50), nullable=False)
    name = Column(String(50), nullable=False)
    widgetType = Column(String, nullable=False)
    jsonData = Column(String, nullable=False)

    __table_args__ = (Index("idx_StyleTuple_key", key, unique=True),)
