from peek_plugin_base.storage.AlembicEnvBase import AlembicEnvBase

from peek_core_screen._private.storage import DeclarativeBase

DeclarativeBase.loadStorageTuples()

alembicEnv = AlembicEnvBase(DeclarativeBase.metadata)
alembicEnv.run()
