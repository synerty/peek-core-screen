from vortex.Tuple import addTupleType, TupleField, Tuple
from peek_core_screen._private.PluginNames import screenTuplePrefix


@addTupleType
class StyleListItemTuple(Tuple):
    """Style List Item Tuple

    This tuple is used to send/receive the list of Styles
    """

    __tupleType__ = screenTuplePrefix + "StyleListItemTuple"

    id: int = TupleField()
    key: str = TupleField()
    name: str = TupleField()
    widgetType: str = TupleField()
