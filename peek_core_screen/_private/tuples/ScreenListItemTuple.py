from vortex.Tuple import addTupleType, TupleField, Tuple
from peek_core_screen._private.PluginNames import screenTuplePrefix


@addTupleType
class ScreenListItemTuple(Tuple):
    """ContainerWidget Tuple

    This tuple is used to send/receive the list of ScreenTuple
    """

    __tupleType__ = screenTuplePrefix + "ScreenListItemTuple"

    id: int = TupleField()
    key: str = TupleField()
    name: str = TupleField()
    screenType: str = TupleField()
