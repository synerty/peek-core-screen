export enum StyleFontFamily {
    "Arial",
    "Times New Roman",
    "Helvetica",
    "Monospace",
}

export enum StyleBorderStyle {
    "Solid",
    "Dashed",
    "Dotted",
}
