import { StyleBorderStyle, StyleFontFamily } from "./admin-style-detail.enums";

const STYLE_CONFIGS = {
    "peek_core_screen.ContainerStyleTuple": [
        {
            name: "backgroundColor",
            formType: "color",
            label: "Background Color",
            shouldShowCallable: () => true,
        },
        {
            name: "fontFamily",
            formType: "select",
            values: StyleFontFamily,
            label: "Font Family",
            shouldShowCallable: () => true,
            isOptional: true,
        },
        {
            name: "fontSize",
            formType: "number",
            label: "Font Size",
            shouldShowCallable: () => true,
        },
        {
            name: "borderStyle",
            formType: "select",
            values: StyleBorderStyle,
            label: "Border Style",
            shouldShowCallable: () => true,
            isOptional: true,
        },
        {
            name: "borderWidth",
            formType: "number",
            label: "Border Width",
            shouldShowCallable: () => true,
        },
        {
            name: "borderColor",
            formType: "color",
            label: "Border Color",
            shouldShowCallable: () => true,
        },
        {
            name: "padding",
            formType: "number",
            label: "Padding",
            shouldShowCallable: () => true,
        },
        {
            name: "margin",
            formType: "number",
            label: "Margin",
            shouldShowCallable: () => true,
        },
    ],
    "peek_core_screen.TextStyleTuple": [
        {
            name: "underline",
            formType: "checkbox",
            label: "Underline",
            shouldShowCallable: () => true,
        },
        {
            name: "bold",
            formType: "checkbox",
            label: "Bold",
            shouldShowCallable: () => true,
        },
        {
            name: "italic",
            formType: "checkbox",
            label: "Italic",
            shouldShowCallable: () => true,
        },
    ],
};

export { STYLE_CONFIGS };
