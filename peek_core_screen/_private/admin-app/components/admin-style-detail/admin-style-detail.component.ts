import { Subscription } from "rxjs";
import { takeUntil } from "rxjs/operators";
import {
    extend,
    NgLifeCycleEvents,
    TupleLoader,
    VortexService,
} from "@synerty/vortexjs";
import { BalloonMsgService } from "@synerty/peek-plugin-base-js";
import {
    ChangeDetectionStrategy,
    Component,
    Input,
    OnChanges,
    SimpleChanges,
} from "@angular/core";
import {
    CheckboxStyleTuple,
    ContainerStyleTuple,
    DatetimeStyleTuple,
    screenFilt,
    StyleTuple,
    TextStyleTuple,
    TupleModel,
} from "@peek/peek_core_screen/_private";
import { STYLE_CONFIGS } from "./admin-style-detail.constants";

interface TupleLoaderFilterI {
    id: number;
    key: string;
}

@Component({
    selector: "admin-style-detail-component",
    templateUrl: "admin-style-detail.component.html",
    styleUrls: ["admin-style-detail.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminStyleDetailComponent
    extends NgLifeCycleEvents
    implements OnChanges
{
    @Input()
    selectedStyleTuple: StyleTuple | null;
    @Input()
    selectedStyleClass: string;

    styleTuple:
        | ContainerStyleTuple
        | TextStyleTuple
        | DatetimeStyleTuple
        | CheckboxStyleTuple;
    styleConfigs = STYLE_CONFIGS;
    styleTypes = TupleModel.styles;

    private readonly filt = { key: "admin.Edit.StyleTuple" };
    private loader: TupleLoader;
    private styleLoaderFilt: TupleLoaderFilterI;
    private loaderSubscription: Subscription;

    constructor(
        private balloonMsg: BalloonMsgService,
        private vortexService: VortexService
    ) {
        super();
        this.styleLoaderFilt = extend(
            this.styleLoaderFilt,
            this.filt,
            screenFilt
        );
        this.loader = this.vortexService.createTupleLoader(
            this,
            this.styleLoaderFilt
        );
    }

    ngOnChanges(change: SimpleChanges) {
        if (
            change.hasOwnProperty("selectedStyleTuple") &&
            change.selectedStyleTuple.currentValue != null
        ) {
            this.styleLoaderFilt.id = change.selectedStyleTuple.currentValue.id;
            if (this.loaderSubscription != null) {
                this.loaderSubscription.unsubscribe();
                this.loaderSubscription = null;
            }
            this.loaderSubscription = this.loader.observable
                .pipe(takeUntil(this.onDestroyEvent))
                .subscribe(async (tuples: StyleTuple[]) => {
                    this.selectedStyleTuple = tuples[0];
                    this.styleTuple =
                        await this.selectedStyleTuple.decodeJsonData();
                });
        }
    }

    newStyle(): void {
        this.selectedStyleTuple = new StyleTuple();
        this.selectedStyleTuple.widgetType = this.selectedStyleClass;
        this.styleTuple = this.styleTypes[this.selectedStyleClass].create();
    }

    /* Save Selected StyleTuple */
    async saveStyle() {
        this.selectedStyleTuple.jsonData = await StyleTuple.encodeJsonData(
            this.styleTuple
        );
        this.loader
            .save([this.selectedStyleTuple])
            .then(() => this.balloonMsg.showSuccess("Save Successful"))
            .catch((e) => this.balloonMsg.showError(e));
    }

    /* Delete Selected StyleTuple */
    deleteStyle(): void {
        if (this.selectedStyleTuple && this.selectedStyleTuple.id) {
            this.loader
                .del([this.selectedStyleTuple])
                .then(() => this.balloonMsg.showSuccess("Delete Successful"))
                .catch((e) => this.balloonMsg.showError(e));
        }
    }

    /* Get enum values */
    getEnumValues(anEnum: any): any {
        return Object.keys(anEnum).filter((type) => !isNaN(<any>type));
    }

    /* Start of check for form types */

    isCheckbox(widget): boolean {
        return (
            widget.formType == "checkbox" &&
            widget.shouldShowCallable(this.styleTuple)
        );
    }

    isTextbox(widget): boolean {
        return (
            widget.formType == "text" &&
            widget.shouldShowCallable(this.styleTuple)
        );
    }

    isDropdown(widget): boolean {
        return (
            widget.formType == "select" &&
            widget.shouldShowCallable(this.styleTuple)
        );
    }

    isStylebox(widget): boolean {
        return (
            widget.formType == "styleTuple" &&
            widget.shouldShowCallable(this.styleTuple)
        );
    }

    isNumberbox(widget): boolean {
        return (
            widget.formType == "number" &&
            widget.shouldShowCallable(this.styleTuple)
        );
    }

    isColor(widget): boolean {
        return (
            widget.formType == "color" &&
            widget.shouldShowCallable(this.styleTuple)
        );
    }

    /* End of check for form types */
}
