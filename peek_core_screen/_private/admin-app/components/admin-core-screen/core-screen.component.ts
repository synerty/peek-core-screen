import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
    selector: "core-screen",
    templateUrl: "core-screen.component.html",
    styleUrls: ["core-screen.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CoreScreenComponent {}
