import { Subject, Subscription } from "rxjs";
import { takeUntil } from "rxjs/operators";
import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    Output,
    SimpleChanges,
} from "@angular/core";
import { NzTreeNode } from "ng-zorro-antd/tree";
import {
    ContainerWidgetTuple,
    ScreenTupleService,
    StyleListItemTuple,
    TextWidgetTuple,
    TupleModel,
} from "@peek/peek_core_screen/_private";
import { WIDGET_CONFIGS } from "./admin-screen-widget.constants";
import { NgLifeCycleEvents, TupleSelector } from "@synerty/vortexjs";

@Component({
    selector: "admin-screen-widget-component",
    templateUrl: "admin-screen-widget.component.html",
    styleUrls: ["admin-screen-widget.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminScreenWidgetComponent extends NgLifeCycleEvents {
    @Input()
    selectedNode: NzTreeNode;

    @Output()
    selectedNodeChange: EventEmitter<NzTreeNode> = new EventEmitter<NzTreeNode>();

    selectedChildNode: string;
    widgetConfigs: object = WIDGET_CONFIGS;
    tupleModel = TupleModel;
    styleTuplesSubscriber: Subscription = null;
    styleListItemTuple = new Subject<StyleListItemTuple[]>();
    createNewChild: boolean = false;

    constructor(private tupleService: ScreenTupleService) {
        super();
    }

    ngOnChanges(change: SimpleChanges) {
        if (change.selectedNode.currentValue) {
            this.loadStyleTuples();
        }
    }

    /* Remove the selected node from the tree */
    removeNode(): void {
        const parentNode = this.selectedNode.parentNode;
        /* Delete the node only if it is not the ROOT node */
        if (parentNode) {
            const originIndex = parentNode.origin.children.findIndex(
                (x) => x.key === this.selectedNode.key
            );
            parentNode.origin.children.splice(originIndex, 1);

            const index = parentNode.children.findIndex(
                (x) => x.key === this.selectedNode.key
            );
            parentNode.children.splice(index, 1);

            this.selectedNode = parentNode;
            this.selectedNode.isSelected = true;
            this.selectedNodeChange.emit(this.selectedNode);
        }
    }

    makeId(length: number = 8) {
        var result = "";
        var characters =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(
                Math.floor(Math.random() * charactersLength)
            );
        }
        return result;
    }

    /* Get selected node type */
    getNodeType(): string {
        return this.selectedNode.origin._tupleName();
    }

    /* Start of check for form types */
    isCheckbox(widget): boolean {
        return (
            widget.formType == "checkbox" &&
            widget.shouldShowCallable(this.selectedNode.origin)
        );
    }

    isTextbox(widget): boolean {
        return (
            widget.formType == "text" &&
            widget.shouldShowCallable(this.selectedNode.origin)
        );
    }

    isDropdown(widget): boolean {
        return (
            widget.formType == "select" &&
            widget.shouldShowCallable(this.selectedNode.origin)
        );
    }

    isStylebox(widget): boolean {
        return (
            widget.formType == "styleTuple" &&
            widget.shouldShowCallable(this.selectedNode.origin)
        );
    }

    isNumberbox(widget): boolean {
        return (
            widget.formType == "number" &&
            widget.shouldShowCallable(this.selectedNode.origin)
        );
    }

    /* End of check for form types */

    /* Get enum values */
    getEnumValues(anEnum: any): any {
        return Object.keys(anEnum).filter((type) => !isNaN(<any>type));
    }

    /* Create new Child Modal */
    handleOkCreateNewChild() {
        this.createNewChild = false;
        this.addChild();
    }

    /* Candel new Child Modal */
    handleCancelCreateNewChild(): void {
        this.createNewChild = false;
    }

    /* Add a child to selected Node */
    addChild(): void {
        let childNode: ContainerWidgetTuple | TextWidgetTuple | null;
        childNode = this.tupleModel.widgets[this.selectedChildNode].create();
        if (childNode != null) {
            childNode.key = this.makeId();
            childNode.title = "New Node";
            const nzTreeNode = new NzTreeNode(childNode);
            this.selectedNode.addChildren([nzTreeNode]);
            this.selectedNode.isExpanded = true;
            this.selectedNode = nzTreeNode;
            this.selectedNode.isSelected = true;
            this.selectedNodeChange.emit(this.selectedNode);
            this.loadStyleTuples();
        }
    }

    /* Show Add Child Modal */
    showAddChild(): void {
        this.createNewChild = true;
    }

    /* Load Style Tuples to populate the dropdown */
    private loadStyleTuples(): any {
        // Select widget type
        const widgetName =
            this.tupleModel.widgets[this.selectedNode.origin._tupleName()];
        const styleTupleListSelector = new TupleSelector(
            StyleListItemTuple.tupleName,
            { widgetType: widgetName.style }
        );
        // Unsubscribe tuplesubscriber if any
        if (this.styleTuplesSubscriber) {
            this.styleTuplesSubscriber.unsubscribe();
            this.styleTuplesSubscriber = null;
        }
        // Observe the StyleListItemTuple for a given widgetType
        this.styleTuplesSubscriber = this.tupleService.observer
            .subscribeToTupleSelector(styleTupleListSelector)
            .pipe(takeUntil(this.onDestroyEvent))
            .subscribe((tuples: StyleListItemTuple[]) => {
                this.styleListItemTuple.next(tuples);
            });
    }
}
