import {
    ContainerWidgetHorizontalAlighment,
    ContainerWidgetOrientation,
    ContainerWidgetVerticalAlignment,
} from "@peek/peek_core_screen/_private";

/*
 The const object WidgetConfigs is used to render different types of
 inputs like textbox, select options and checkbox.
 
 The "name" property should match the Tuples' properties. For example
 ContainerWidgetTuple.ts has properties "key", "title" etc and hence it is defined here
 under "peek_core_screen.ContainerWidgetTuple"
 */

const WIDGET_CONFIGS: object = {
    /* The name should match the name of plugin (peek_core_screen) + Tuple in
     plugin-module/_private/tuples/** /*Tuple.ts's tupleName property
     */
    "peek_core_screen.ContainerWidgetTuple": [
        {
            name: "title",
            formType: "text",
            label: "Title",
            shouldShowCallable: () => true,
        },
        {
            name: "orientation",
            formType: "select",
            label: "Orientation",
            values: ContainerWidgetOrientation,
            shouldShowCallable: () => true,
            isOptional: false,
        },
        {
            name: "horizontalAlign",
            formType: "select",
            label: "Horizontal Alignment",
            values: ContainerWidgetHorizontalAlighment,
            shouldShowCallable: (selectedNodeOrigin) =>
                selectedNodeOrigin.orientation ==
                ContainerWidgetOrientation.Horizontal,
            isOptional: true,
        },
        {
            name: "verticalAlign",
            formType: "select",
            label: "Vertical Alignment",
            values: ContainerWidgetVerticalAlignment,
            shouldShowCallable: (selectedNodeOrigin) =>
                selectedNodeOrigin.orientation ==
                ContainerWidgetOrientation.Vertical,
            isOptional: true,
        },
        {
            name: "columns",
            formType: "number",
            label: "Columns",
            shouldShowCallable: (selectedNodeOrigin) =>
                selectedNodeOrigin.orientation ==
                ContainerWidgetOrientation.Vertical,
        },
        {
            name: "styleKey",
            formType: "styleTuple",
            label: "Style",
            shouldShowCallable: () => true,
            isOptional: true,
        },
    ],
    "peek_core_screen.TextWidgetTuple": [
        {
            name: "title",
            formType: "text",
            label: "Title",
            shouldShowCallable: () => true,
        },
        {
            name: "label",
            formType: "text",
            label: "Label",
            shouldShowCallable: () => true,
        },
        {
            name: "bindText",
            formType: "text",
            label: "Bind Text",
            shouldShowCallable: () => true,
        },
        {
            name: "styleKey",
            formType: "styleTuple",
            label: "Style",
            shouldShowCallable: () => true,
            isOptional: true,
        },
    ],
};

export { WIDGET_CONFIGS };
