import { ChangeDetectionStrategy, Component, Input } from "@angular/core";

@Component({
    selector: "admin-screen-preview-component",
    templateUrl: "admin-screen-preview.component.html",
    styleUrls: ["admin-screen-preview.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminScreenPreviewComponent {
    @Input()
    screenTupleByKey: string;
}
