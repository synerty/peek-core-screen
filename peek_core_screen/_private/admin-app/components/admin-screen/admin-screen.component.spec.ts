import { AdminScreenComponent } from "./admin-screen.component";
import { IconDefinition } from "@ant-design/icons-angular";
import {
    FolderOpenFill,
    FolderOpenOutline,
} from "@ant-design/icons-angular/icons";

const icons: IconDefinition[] = [FolderOpenOutline, FolderOpenFill];

describe("AdminScreenComponent", () => {});
