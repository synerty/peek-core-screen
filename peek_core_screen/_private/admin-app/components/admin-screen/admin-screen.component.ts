import { BehaviorSubject, Subscription } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ChangeDetectionStrategy, Component } from "@angular/core";
import { NzTreeNode } from "ng-zorro-antd/tree";
import { BalloonMsgService } from "@synerty/peek-plugin-base-js";
import {
    extend,
    NgLifeCycleEvents,
    TupleLoader,
    TupleSelector,
    VortexService,
} from "@synerty/vortexjs";
import {
    ContainerWidgetTuple,
    screenFilt,
    ScreenListItemTuple,
    ScreenTuple,
} from "@peek/peek_core_screen/_private";
import {
    ScreenTupleService,
    TupleLoaderFilterI,
} from "@peek/peek_core_screen/_private/ScreenTupleService";

@Component({
    selector: "admin-screen",
    templateUrl: "admin-screen.component.html",
    styleUrls: ["admin-screen.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminScreenComponent extends NgLifeCycleEvents {
    screenListItemTuples: ScreenListItemTuple[] = [];
    selectedScreenTuple: ScreenTuple;
    previewScreenTupleKey: { [key: string]: string };
    createNewScreen: boolean = false;
    screen: any;

    selectedNode$ = new BehaviorSubject<NzTreeNode | null>(null);
    rootNodes$ = new BehaviorSubject<any[]>([]);

    private readonly filt = { key: "admin.Edit.ScreenTuple" };
    private loader: TupleLoader;
    private loaderSubscription: Subscription;
    private screenLoaderFilt: TupleLoaderFilterI;

    constructor(
        private balloonMsg: BalloonMsgService,
        private vortexService: VortexService,
        private tupleService: ScreenTupleService
    ) {
        super();

        // Create a Tuple Selector
        const screenTupleListSelector = new TupleSelector(
            ScreenListItemTuple.tupleName,
            { screenType: ScreenTuple.detailScreen }
        );

        // Observe for new ScreenListItemTuple
        this.tupleService.observer
            .subscribeToTupleSelector(screenTupleListSelector)
            .pipe(takeUntil(this.onDestroyEvent))
            .subscribe((tuples: ScreenListItemTuple[]) => {
                this.screenListItemTuples = tuples;
            });

        // Create a Screen Loader Filter - Load an individual ScreenTuple based on filter
        this.screenLoaderFilt = extend(
            this.screenLoaderFilt,
            this.filt,
            screenFilt
        );
        this.loader = this.vortexService.createTupleLoader(
            this,
            this.screenLoaderFilt
        );
    }

    get selectedNode() {
        return this.selectedNode$.getValue();
    }

    set selectedNode(value) {
        this.selectedNode$.next(value);
    }

    get rootNodes() {
        return this.rootNodes$.getValue();
    }

    set rootNodes(value) {
        this.rootNodes$.next(value);
    }

    /* Save the current screen tuple using TupleLoader */
    async saveScreen(): Promise<any> {
        this.selectedScreenTuple.encodedContainerWidget =
            await ScreenTuple.encodeContainerWidget(this.rootNodes[0]);
        this.loader
            .save([this.selectedScreenTuple])
            .then(() => {
                this.previewScreenTupleKey = {
                    key: this.selectedScreenTuple.key,
                };
                this.balloonMsg.showSuccess("Save Successful");
            })
            .catch((e) => this.balloonMsg.showError(e));
    }

    /* Discard the current changes and reload */
    resetScreen(): void {
        this.loader
            .load()
            .then(() => this.balloonMsg.showSuccess("Reset Successful"))
            .catch((e) => this.balloonMsg.showError(e));
    }

    /* Callback for choosing the selectedScreenTuple */
    selectCurrentScreen(item: ScreenListItemTuple) {
        if (item && item.id) {
            this.screenLoaderFilt.id = item.id;
            this.loadScreenTuple();
        }
    }

    /* Callback for selecting a particular node in a tree */
    selectNode(node): void {
        if (node != null) {
            this.selectedNode = node;
            this.selectedNode.isSelected = true;
        } else {
            this.selectedNode = null;
        }
    }

    /* Start of Create New Screen member functions */
    createScreen(): void {
        this.selectedScreenTuple = new ScreenTuple();
        this.createNewScreen = true;
    }

    async handleOkCreateScreen() {
        this.createNewScreen = false;
        if (
            this.selectedScreenTuple.key.length > 0 &&
            this.selectedScreenTuple.name.length > 0
        ) {
            this.selectedScreenTuple =
                await ScreenTuple.createDetailScreenTuple(
                    this.selectedScreenTuple.key,
                    this.selectedScreenTuple.name
                );
            const rootContainerWidget =
                ContainerWidgetTuple.createRootContainerWidget(
                    this.selectedScreenTuple.key,
                    this.selectedScreenTuple.name
                );
            this.rootNodes = [rootContainerWidget];
            this.saveScreen();
        }
    }

    handleCancelCreateScreen(): void {
        this.createNewScreen = false;
    }

    /* End of Create New Screen member functions */

    /* Load A ScreenTuple on TupleItemList Selector */
    private loadScreenTuple(): void {
        // Create an observable to load an individual ScreenTuple
        if (this.loaderSubscription) {
            this.loaderSubscription.unsubscribe();
            this.loaderSubscription = null;
        }
        this.loaderSubscription = this.loader.observable
            .pipe(takeUntil(this.onDestroyEvent))
            .subscribe(async (tuples: ScreenTuple[]) => {
                if (tuples.length == 1) {
                    this.selectedScreenTuple = tuples[0];
                    this.previewScreenTupleKey = {
                        key: this.selectedScreenTuple.key,
                    };
                    const containerWidget =
                        await this.selectedScreenTuple.decodeContainerWidget();
                    this.rootNodes = [containerWidget];
                    this.selectedNode = null;
                }
            });
    }
}
