import { TestBed } from "@angular/core/testing";
import { AdminStyleComponent } from "./admin-style.component";
import { VortexService, VortexStatusService } from "@synerty/vortexjs";
import { BalloonMsgService } from "@synerty/peek-plugin-base-js";
import { CoreScreenModule } from "../../core-screen.module";

describe("AdminStyleComponent", () => {
    let component: AdminStyleComponent;

    beforeEach(() => {
        const protocol =
            location.protocol.toLowerCase() == "https:" ? "wss" : "ws";
        // VortexService.setVortexUrl(`${protocol}://${location.hostname}:${location.port}/vortexws`);
        VortexService.setVortexUrl(`ws://localhost:8010/vortexws`);
        VortexService.setVortexClientName("peek-admin-app");
        TestBed.configureTestingModule({
            imports: [CoreScreenModule],
            providers: [VortexService, VortexStatusService, BalloonMsgService],
            declarations: [],
        }).compileComponents();
    });

    it("should load StyleListItemTuples on selecting StyleClass", () => {
        const fixture = TestBed.createComponent(AdminStyleComponent);
        fixture.detectChanges();
        const componentInstance = fixture.debugElement.componentInstance;
        expect(componentInstance).toBeTruthy();
        console.log(componentInstance.styleTypes);
    });
});
