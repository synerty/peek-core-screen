import { ChangeDetectionStrategy, Component } from "@angular/core";
import { Subscription } from "rxjs";
import { takeUntil } from "rxjs/operators";
import {
    ScreenTupleService,
    StyleListItemTuple,
    TupleModel,
} from "@peek/peek_core_screen/_private";
import { NgLifeCycleEvents, TupleSelector } from "@synerty/vortexjs";

@Component({
    selector: "admin-style",
    templateUrl: "admin-style.component.html",
    styleUrls: ["admin-style.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminStyleComponent extends NgLifeCycleEvents {
    styleTypes = TupleModel.styles;
    styleListItemTuples: StyleListItemTuple[];
    selectedStyleClass: string | null;
    selectedStyleTuple: StyleListItemTuple;
    styleTuplesSubscriber: Subscription = null;

    constructor(private tupleService: ScreenTupleService) {
        super();
    }

    /* Select the Style Type */
    selectStyleType(item: string) {
        this.selectedStyleTuple = null;
        this.selectedStyleClass = item;
        this.subscribeStyleTupleLoader();
    }

    /* Select StyleTuple from the list */
    selectStyleTuple(tuple: StyleListItemTuple) {
        this.selectedStyleTuple = tuple;
    }

    /* Subscribe to StyleListItemTuple on Selecting the Style Type */
    private subscribeStyleTupleLoader(): void {
        // Unsubscribe any previous subscriptions
        if (this.styleTuplesSubscriber != null) {
            this.styleTuplesSubscriber.unsubscribe();
            this.styleTuplesSubscriber = null;
        }

        // Set the TupleSelector
        const styleTupleListSelector = new TupleSelector(
            StyleListItemTuple.tupleName,
            { widgetType: this.selectedStyleClass }
        );

        // Subscribe to StyleListItemTuples
        this.styleTuplesSubscriber = this.tupleService.observer
            .subscribeToTupleSelector(styleTupleListSelector)
            .pipe(takeUntil(this.onDestroyEvent))
            .subscribe((tuples: StyleListItemTuple[]) => {
                this.styleListItemTuples = tuples;
            });
    }
}
