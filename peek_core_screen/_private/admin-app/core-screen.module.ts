import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { NzLayoutModule } from "ng-zorro-antd/layout";
import { NzGridModule } from "ng-zorro-antd/grid";
import { NzTreeModule } from "ng-zorro-antd/tree";
import { NzIconModule } from "ng-zorro-antd/icon";
import { NzSelectModule } from "ng-zorro-antd/select";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzModalModule } from "ng-zorro-antd/modal";
import { NzInputModule } from "ng-zorro-antd/input";
import { NzFormModule } from "ng-zorro-antd/form";
import { NzCheckboxModule } from "ng-zorro-antd/checkbox";
import { NzListModule } from "ng-zorro-antd/list";
import { NzDividerModule } from "ng-zorro-antd/divider";
import { NzSwitchModule } from "ng-zorro-antd/switch";
import { NzInputNumberModule } from "ng-zorro-antd/input-number";
import { CoreDynamicScreenModule } from "@peek/peek_core_screen/core-dynamic-screen";
import { HttpClientModule } from "@angular/common/http";
import {
    AdminScreenComponent,
    AdminScreenPreviewComponent,
    AdminScreenWidgetComponent,
    AdminStyleComponent,
    AdminStyleDetailComponent,
    CoreScreenComponent,
} from "./components";

// Define the routes for this Angular module.
export const pluginRoutes: Routes = [
    {
        path: "",
        pathMatch: "full",
        component: CoreScreenComponent,
    },
];

@NgModule({
    declarations: [
        AdminScreenComponent,
        AdminScreenWidgetComponent,
        AdminScreenPreviewComponent,
        AdminStyleComponent,
        AdminStyleDetailComponent,
        CoreScreenComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(pluginRoutes),
        HttpClientModule,
        FormsModule,
        NzLayoutModule,
        NzGridModule,
        NzTreeModule,
        NzIconModule,
        NzInputModule,
        NzSelectModule,
        NzButtonModule,
        NzModalModule,
        NzFormModule,
        NzCheckboxModule,
        NzListModule,
        NzDividerModule,
        NzSwitchModule,
        NzInputNumberModule,
        CoreDynamicScreenModule,
    ],
})
export class CoreScreenModule {}
