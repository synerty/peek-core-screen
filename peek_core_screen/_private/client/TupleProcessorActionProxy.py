from peek_plugin_base.PeekVortexUtil import peekServerName
from peek_core_screen._private.PluginNames import screenFilt
from peek_core_screen._private.PluginNames import screenActionProcessorName
from vortex.handler.TupleActionProcessorProxy import TupleActionProcessorProxy


def makeTupleActionProcessorProxy():
    return TupleActionProcessorProxy(
        tupleActionProcessorName=screenActionProcessorName,
        proxyToVortexName=peekServerName,
        additionalFilt=screenFilt,
    )
