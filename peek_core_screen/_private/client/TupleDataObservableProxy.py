from peek_plugin_base.PeekVortexUtil import peekServerName
from peek_core_screen._private.PluginNames import screenFilt
from peek_core_screen._private.PluginNames import screenObservableName
from vortex.handler.TupleDataObservableProxyHandler import (
    TupleDataObservableProxyHandler,
)


def makeDeviceTupleDataObservableProxy():
    return TupleDataObservableProxyHandler(
        observableName=screenObservableName,
        proxyToVortexName=peekServerName,
        additionalFilt=screenFilt,
    )
