from vortex.handler.TupleDataObservableHandler import TupleDataObservableHandler

from peek_core_screen._private.PluginNames import screenFilt
from peek_core_screen._private.PluginNames import screenObservableName
from peek_core_screen._private.server.tuple_providers.StyleListItemTupleProvider import (
    StyleListItemTupleProvider,
)
from peek_core_screen._private.server.tuple_providers.StyleTupleProvider import (
    StyleTupleProvider,
)
from peek_core_screen._private.storage.ScreenTuple import ScreenTuple
from peek_core_screen._private.storage.StyleTuple import StyleTuple
from peek_core_screen._private.tuples.ScreenListItemTuple import ScreenListItemTuple
from peek_core_screen._private.server.tuple_providers.ScreenTupleProvider import (
    ScreenTupleProvider,
)
from peek_core_screen._private.server.tuple_providers.ScreenListItemTupleProvider import (
    ScreenListItemTupleProvider,
)
from peek_core_screen._private.tuples.StyleListItemTuple import StyleListItemTuple


def makeTupleDataObservableHandler(ormSessionCreator):
    """ " Make Tuple Data Observable Handler

    This method creates the observable object, registers the tuple providers and then
    returns it.

    :param ormSessionCreator: A function that returns a SQLAlchemy session when called

    :return: An instance of :code:`TupleDataObservableHandler`

    """
    tupleObservable = TupleDataObservableHandler(
        observableName=screenObservableName, additionalFilt=screenFilt
    )

    # Register TupleProviders
    tupleObservable.addTupleProvider(
        ScreenTuple.tupleName(), ScreenTupleProvider(ormSessionCreator)
    )
    tupleObservable.addTupleProvider(
        ScreenListItemTuple.tupleName(), ScreenListItemTupleProvider(ormSessionCreator)
    )
    tupleObservable.addTupleProvider(
        StyleTuple.tupleName(), StyleTupleProvider(ormSessionCreator)
    )
    tupleObservable.addTupleProvider(
        StyleListItemTuple.tupleName(), StyleListItemTupleProvider(ormSessionCreator)
    )

    return tupleObservable
