from peek_core_screen._private.server.controller.MainController import MainController
from peek_core_screen._private.storage import ScreenTuple
from peek_core_screen.server.ImportScreenApiABC import ImportScreenApiABC


class ImportScreenApi(ImportScreenApiABC):
    def __init__(self, mainController: MainController):
        self._mainController = mainController

    def doSomethingGood(self, somethingsDescription: str) -> ScreenTuple:
        """TODO"""
        # Here we could pass on the request to the self._mainController if we wanted.
        # EG self._mainController.somethingCalled(somethingsDescription)
        return ScreenTuple()

    def shutdown(self):
        pass
