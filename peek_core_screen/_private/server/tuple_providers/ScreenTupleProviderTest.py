from peek_core_screen._private.tuples import loadPrivateTuples
from peek_core_screen.tuples import loadPublicTuples
from twisted.internet.defer import inlineCallbacks
from twisted.trial import unittest
from vortex.TupleSelector import TupleSelector
from peek_plugin_base.server.test_mixins.PluginDatabaseTestMixin import (
    PluginDatabaseTestMixin,
)
from peek_core_screen._private.server.tuple_providers.ScreenTupleProvider import (
    ScreenTupleProvider,
)
from peek_core_screen._private import alembic
from peek_core_screen._private.storage.DeclarativeBase import metadata
from twisted.python.threadable import isInIOThread
import logging
import mock

logger = logging.getLogger(__name__)


class ScreenTupleProviderTest(unittest.TestCase, PluginDatabaseTestMixin):
    def setUp(self) -> None:
        """
        Setup conection to database
        """
        self.setUpDbConnection(alembic, metadata)
        loadPublicTuples()
        loadPrivateTuples()

    def tearDown(self) -> None:
        """
        Tear down DB connection
        """
        self._db.closeAllSessions()

    @inlineCallbacks
    @mock.patch(isInIOThread, return_value=True)
    def test_getScreenTupleTest(self):
        """
        Test getting Screen Tuple Recursively
        :return:
        """
        filt = {
            "subscribe": True,
            "name": "peek_core_screen",
            "key": "tupleDataObservable",
            "plugin": "peek_core_screen",
            "tupleSelector": {
                "name": "peek_core_screen.ScreenTuple",
                "selector": {"id": 20},
            },
            "unsubscribe": False,
            "disableCache": False,
        }

        tupleSelector = TupleSelector(
            name="peek_core_screen.ScreenTuple", selector={"id": 20}
        )

        tupleProvider = ScreenTupleProvider(self._db.ormSessionCreator)
        vortexMsg = yield tupleProvider.makeVortexMsg(filt, tupleSelector)

        self.assertTrue(vortexMsg is not None)
