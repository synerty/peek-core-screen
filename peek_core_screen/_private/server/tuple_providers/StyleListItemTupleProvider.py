from txhttputil.util.DeferUtil import deferToThreadWrap
from typing import Union

from twisted.internet.defer import Deferred

from vortex.Payload import Payload
from vortex.TupleSelector import TupleSelector
from vortex.handler.TupleDataObservableHandler import TuplesProviderABC

from peek_core_screen._private.tuples.StyleListItemTuple import StyleListItemTuple
from peek_core_screen._private.storage.StyleTuple import StyleTuple


class StyleListItemTupleProvider(TuplesProviderABC):
    def __init__(self, ormSessionCreator):
        self._ormSessionCreator = ormSessionCreator

    @deferToThreadWrap
    def makeVortexMsg(
        self, filt: dict, tupleSelector: TupleSelector
    ) -> Union[Deferred, bytes]:
        widgetType = tupleSelector.selector["widgetType"]
        session = self._ormSessionCreator()
        try:
            qry = session.query(
                StyleTuple.id, StyleTuple.key, StyleTuple.name, StyleTuple.widgetType
            ).filter(StyleTuple.widgetType == widgetType)
            tupleList = [
                StyleListItemTuple(
                    id=o.id, key=o.key, name=o.name, widgetType=o.widgetType
                )
                for o in qry
            ]
            # Create the vortex message
            return Payload(filt, tuples=tupleList).makePayloadEnvelope().toVortexMsg()
        finally:
            session.close()
