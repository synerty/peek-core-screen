import logging

from txhttputil.util.DeferUtil import deferToThreadWrap
from typing import Union
from twisted.internet.defer import Deferred
from sqlalchemy.orm.exc import NoResultFound
from vortex.DeferUtil import deferToThreadWrapWithLogger

from vortex.Payload import Payload
from vortex.TupleSelector import TupleSelector
from vortex.handler.TupleDataObservableHandler import TuplesProviderABC

from peek_core_screen._private.storage.StyleTuple import StyleTuple
from peek_core_screen._private.storage.ScreenTuple import ScreenTuple

logger = logging.getLogger(__name__)


class ScreenTupleProvider(TuplesProviderABC):
    """
    ScreenTupleProvider : Provides Screen Tuple based on filter by ScreenTuple's ID or Key
    """

    def __init__(self, ormSessionCreator):
        self._ormSessionCreator = ormSessionCreator

    @deferToThreadWrapWithLogger(logger)
    def makeVortexMsg(
        self, filt: dict, tupleSelector: TupleSelector
    ) -> Union[Deferred, bytes]:

        session = self._ormSessionCreator()
        try:
            try:
                # Query Screen Tuple based on filter
                if "id" in tupleSelector.selector:
                    data = self._getTupleById(session, tupleSelector.selector["id"])
                elif "key" in tupleSelector.selector:
                    data = self._getTupleByKey(session, tupleSelector.selector["key"])

                # Decode Container Widget
                containerWidgetTuple = (
                    Payload().fromEncodedPayload(data.encodedContainerWidget).tuples[0]
                )

                data.styleByKey = {}

                # Load Style Tuple data recursively into data.StyleByKey dict
                self._addStyleTuplesToContainer(
                    session, containerWidgetTuple, data.styleByKey
                )

            except NoResultFound:
                data = []

            return Payload(filt, tuples=data).makePayloadEnvelope().toVortexMsg()

        finally:
            session.close()

    def _addStyleTuplesToContainer(self, session, widgetTuple, styleByKey):
        """
        Add Style Tuple To styleByKey List
        :param session: Database session
        :param widgetTuple: WidgetTuple (ex: ContainerWidgetTuple, TextWidgetTuple, etc..)
        :param styleByKey: StyleByKey contains the dict of Style Tuples
        :return: None
        """
        if widgetTuple.styleKey and widgetTuple.styleKey not in styleByKey:
            try:
                styleTuple = (
                    session.query(StyleTuple)
                    .filter(StyleTuple.key == widgetTuple.styleKey)
                    .one()
                )
                styleByKey[widgetTuple.styleKey] = styleTuple
            except NoResultFound:
                pass

        if hasattr(widgetTuple, "children") and widgetTuple.children is not None:
            for childTuple in widgetTuple.children:
                self._addStyleTuplesToContainer(session, childTuple, styleByKey)

    def _getTupleById(self, session, id):
        return session.query(ScreenTuple).filter(ScreenTuple.id == id).one()

    def _getTupleByKey(self, session, key):
        return session.query(ScreenTuple).filter(ScreenTuple.key == key).one()
