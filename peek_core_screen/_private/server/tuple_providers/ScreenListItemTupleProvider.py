from txhttputil.util.DeferUtil import deferToThreadWrap
from typing import Union

from twisted.internet.defer import Deferred

from vortex.Payload import Payload
from vortex.TupleSelector import TupleSelector
from vortex.handler.TupleDataObservableHandler import TuplesProviderABC

from peek_core_screen._private.tuples.ScreenListItemTuple import ScreenListItemTuple
from peek_core_screen._private.storage.ScreenTuple import ScreenTuple


class ScreenListItemTupleProvider(TuplesProviderABC):
    def __init__(self, ormSessionCreator):
        self._ormSessionCreator = ormSessionCreator

    @deferToThreadWrap
    def makeVortexMsg(
        self, filt: dict, tupleSelector: TupleSelector
    ) -> Union[Deferred, bytes]:
        screenType = tupleSelector.selector["screenType"]
        session = self._ormSessionCreator()
        try:
            qry = session.query(
                ScreenTuple.id,
                ScreenTuple.key,
                ScreenTuple.name,
                ScreenTuple.screenType,
            ).filter(ScreenTuple.screenType == screenType)
            tupleList = [
                ScreenListItemTuple(
                    id=o.id, key=o.key, name=o.name, screenType=o.screenType
                )
                for o in qry
            ]
            # Create the vortex message
            return Payload(filt, tuples=tupleList).makePayloadEnvelope().toVortexMsg()
        finally:
            session.close()
