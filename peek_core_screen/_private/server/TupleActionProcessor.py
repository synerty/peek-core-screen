from vortex.handler.TupleActionProcessor import TupleActionProcessor

from peek_core_screen._private.PluginNames import screenFilt
from peek_core_screen._private.PluginNames import screenActionProcessorName
from .controller.MainController import MainController


def makeTupleActionProcessorHandler(mainController: MainController):
    processor = TupleActionProcessor(
        tupleActionProcessorName=screenActionProcessorName,
        additionalFilt=screenFilt,
        defaultDelegate=mainController,
    )
    return processor
