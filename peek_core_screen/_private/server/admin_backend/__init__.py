from .ScreenTableHandler import makeScreenTableHandler
from .StyleTableHandler import makeStyleTableHandler

from vortex.handler.TupleDataObservableHandler import TupleDataObservableHandler


def makeAdminBackendHandlers(
    tupleObservable: TupleDataObservableHandler, dbSessionCreator
):
    yield makeScreenTableHandler(tupleObservable, dbSessionCreator)
    yield makeStyleTableHandler(tupleObservable, dbSessionCreator)
