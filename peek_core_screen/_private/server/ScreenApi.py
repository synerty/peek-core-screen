from peek_core_screen._private.server.controller.MainController import MainController
from peek_core_screen._private.storage.ScreenTuple import ScreenTuple
from peek_core_screen.server.ImportScreenApiABC import ImportScreenApiABC


class CoreScreenApi(ImportScreenApiABC):
    def __init__(self, mainController: MainController):
        self._mainController = mainController

    def doSomethingGood(self, somethingsDescription: str) -> ScreenTuple:
        """TODO"""

        # TODO
        return ScreenTuple()

    def shutdown(self):
        pass
